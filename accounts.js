///////////////////////////////////////////////////////////////////////
//        Construcción de aplicaciones REST Practitioner             //
///////////////////////////////////////////////////////////////////////
//     Fernando Boroukhovitch - BBVA Uruguay - Octubre de 2018       //
///////////////////////////////////////////////////////////////////////
//           Módulo ACCOUNTS - Peticiones REST de Cuentas            //
///////////////////////////////////////////////////////////////////////

//Definición de variables y objetos requeridos
var connectParms = require('./params.js');
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
app.use(bodyParser.json());

//Importación constantes globales
const port = connectParms.portAccounts;
const baseMLabURL = connectParms.baseMLabURL.valor;
const apikeyMLab = connectParms.apikeyMLab.valor;
const URI = connectParms.URI;

  /////////////////////////////////
 //  Consultas y ABM ACCOUNTS   //
/////////////////////////////////

//GET /accounts consumiendo API REST de mLab - Obtiene todas las cuentas existentes
app.get(URI + 'accounts',
  function(req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/accounts.');
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('account?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": "Error al obtener las cuentas."
            };
            res.status(500); //Error interno del servidor
        } else {
            if(body.length > 0) {
                response = body;
            } else {
                response = {
                  "msg":"No existen cuentas."
                };
                res.status(404); //Cuentas no encontradas
            };
        };
        res.send(response); //Respuesta OK - Se devuelve/n cuenta/s
      });
      console.log('Ejecución GET /accounts finalizada.');
  });

//GET /accounts con ID consumiendo API REST de mLab - Obtiene una cuenta específica
app.get(URI + 'accounts/:idAccount',
  function (req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/accounts/:id (con ID).');
    let idAccount = req.params.idAccount;
    console.log("Id cuenta: " + idAccount);
    var queryString = 'q={"id_account":' + idAccount + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('account?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
          response = {
            "msg":"Error obteniendo cuenta."
          };
          res.status(500); //Error interno del servidor
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg":"Cuenta no encontrada."
            };
            res.status(404); //Cuenta no encontrada
          };
        };
        res.send(response); //Respuesta OK - Se devuelve cuenta
      });
      console.log('Ejecución GET /api-uruguay/v1/accounts/:id (con ID) finalizada.');
  });

//POST /users mLab (datos recibidos en el Body) - Inserta un nueva cuenta,
//controlando previamente que el ID de la cuenta no exista en la base de datos, collection ACCOUNT.
app.post(URI + 'accounts',
  function(req, res){
    console.log('Ejecutando petición POST /api-uruguay/v1/accounts.');
    let idAccount = req.body.id_account;
    console.log("Id cuenta a crear: " + req.body.id_account);
    var queryString = 'q={"id_account":' + idAccount + '}&';
    var queryStrField = 'f={"_id":0}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('account?' + queryString + queryStrField + apikeyMLab,
      function(error, respuestaMLab, body){
        if (error){
            res.status(500); //Error interno del servidor
            res.send({
              "msg":"Error verificando existencia de ID de cuenta recibida."
            });
        } else {
          if(body.length > 0) {
            console.log('La cuenta ya existe. Cuenta no creada.');
            res.status(400); //Error en los datos de la cuenta proporcionados
            res.send({
              "msg":"La cuenta con Id = " + idAccount + " ya existe. No se ha creado la cuenta."
            });
          } else {
            clienteMlab.post(baseMLabURL + "account?" + apikeyMLab, req.body,
              function(errorP, respuestaMLab, bodyP){
                if (!errorP) {
                  console.log('Cuenta creada exitosamente.');
                  res.status(200); //OK Alta de cuenta realizada con éxito
                  res.send(bodyP);
                } else {
                  console.log('Error al crear la cuenta.');
                  res.status(500); //Error de servidor al insertar nuevo cuenta
                  res.send({"msg":"Error al crear la cuenta."});
                };
              });
          };
        };
      });
      console.log('Ejecución POST /api-uruguay/v1/accounts finalizada.');
  });

//PUT /accounts (datos recibidos en el Body) - Actualiza una cuenta específica
app.put(URI + "accounts/:idAccount",
  function(req, res){
    console.log('Ejecutando petición PUT /api-uruguay/v1/accounts/:idAccount.');
    let idAccount = req.params.idAccount;
    console.log("Id cuenta a actualizar: " + idAccount);
    var queryStringID = 'q={"id_account":' + idAccount + '}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('account?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        if (respuesta != undefined) {
            var actualizaAccount = {
              'id_account':+idAccount,
              'id_user':req.body.id_user,
              'currency':req.body.currency,
              'balance':req.body.balance,
              'transactions':req.body.transactions
            };
            clienteMlab.put('account?q={"id_account": ' + respuesta.id_account + '}&' + apikeyMLab, actualizaAccount,
            function(errorP, respuestaMLab,bodyP){
              if (!errorP) {
                  console.log('Cuenta actualizada exitosamente.');
                  res.status(200); //Actualización exitosa
                  res.send({"msg":"Cuenta actualizada exitosamente."});
              } else {
                  console.log('Error al actualizar la cuenta.');
                  res.status(500); //Error de servidor al actualizar la cuenta
                  res.send({"msg":"Error al actualizar la cuenta."});
              };
            });
        } else {
            console.log('El Id de cuenta a actualizar no existe.');
            res.status(404); //Id de cuenta no existente
            res.send({"msg":"El Id de cuenta a actualizar no existe."});
        };
      });
      console.log('Ejecución PUT /api-uruguay/v1/accounts/:idAccount finalizada.');
  });

  // POST /accounts/{idAccount}/transactions - Crea una nueva transacción
  app.post(URI + "accounts/:idAccount/transactions",
    function (req, res){
      console.log('Ejecutando petición POST /api-uruguay/v1/accounts/{idAccount}/transactions/{idTransaccion}.');
      let idAccount = req.params.idAccount;
      console.log(idAccount);
      var queryString = 'q={"id_account":' + idAccount + '}&';
      var  clienteMlab = requestJSON.createClient(baseMLabURL);
      clienteMlab.get('account?'+ queryString + apikeyMLab,
        function(error, respuestaMLab , body) {
          var respuesta = body[0];
          console.log(respuesta);
          if(respuesta != undefined){
            var saldoActual = +respuesta.balance;
            var importeTrans = +req.body.transactions.amount;
            var saldoResultado = saldoActual + importeTrans;
            if (saldoResultado >= 0) {
              console.log("Saldo suficiente.");
              var nuevoSaldo = {
                            "balance": saldoResultado,
                            "transactions": [req.body.transactions]
                        };
              var modificaCuenta = '{"$set":' + JSON.stringify(nuevoSaldo) + '}';
              clienteMlab.put('account?q={"id_account": ' + respuesta.id_account + '}&' + apikeyMLab, JSON.parse(modificaCuenta),
                function(errorP, respuestaMLabP, bodyP) {
                  res.status(200); //Password actualizado correctamente.
                  res.send({"msg":"Transacción realizada exitosamente. El saldo de la cuenta ha sido actualizado."});
                });
            } else {
              console.log("Saldo insuficiente.");
              res.status(401); //Transacciòn no autorizada
              res.send({"msg":"El saldo de la cuenta es insuficiente. Transacción no realizada."});
            };
          } else {
            console.log("Cuenta incorrecta.");
            res.status(401); //Cuenta incorrecta
            res.send({"msg":"Cuenta incorrecta. Transacción no realizada."});
          };
        });
        console.log('Ejecución POST /api-uruguay/v1/accounts/{idAccount}/transactions.');;
    });


//DELETE /accounts - Elimina una cuenta específica
app.delete(URI + "accounts/:idAccount",
  function(req, res){
    console.log('Ejecutando petición DELETE /api-uruguay/v1/accounts/:idAccount.');
    let idAccount = req.params.idAccount;
    console.log("Id cuenta a eliminar: " + idAccount);
    var queryStringID = 'q={"id_account":' + idAccount + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('account?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        if (respuesta != undefined) {
          console.log('Cuenta eliminada exitosamente.');
          httpClient.delete(baseMLabURL + "account/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(errorD, respuestaMLab,bodyD){
              res.status(200); //Id de cuenta eliminado correctamente
              res.send({"msg":"Cuenta eliminada exitosamente."});
            });
        } else {
          console.log('El Id de cuenta a eliminar no existe.');
          res.status(404); //Id de cuenta no existente
          res.send({"msg":"El Id de cuenta a eliminar no existe."});
        };
      });
      console.log('Ejecución DELETE /api-uruguay/v1/accounts/:idAccount finalizada.');
  });

//Habilita recepciòn de peticiones por el puerto configurado en param.js
app.listen(port);
console.log('Servicio ACCOUNTS:       Escuchando en el puerto ' + port);
