///////////////////////////////////////////////////////////////////////
//           Construcción de aplicaciones REST Practitioner          //
///////////////////////////////////////////////////////////////////////
//     Fernando Boroukhovitch - BBVA Uruguay - Octubre de 2018       //
///////////////////////////////////////////////////////////////////////
//          Módulo PARAMS - Definición de variables globales         //
///////////////////////////////////////////////////////////////////////

//Definición de variables globales
var portSecurity = process.env.port_security || 3000;
var portUsers = process.env.port_users || 3001;
var portAccounts = process.env.port_accounts || 3002;
const URI = '/api-uruguay/v1/';

//Valores para string de conexión a BB.DD. MLab MongoDB
const baseMLabURL = { valor : "https://api.mlab.com/api/1/databases/techubduruguay/collections/" };
const apikeyMLab = { valor : "apiKey=fJ6_Z5W6wscyZ6IreSOOqoCyjKir59qS" };

//Exportaciòn de variables globales para reutilización en otros módulos
module.exports.baseMLabURL = baseMLabURL;
module.exports.apikeyMLab = apikeyMLab;
module.exports.portSecurity = portSecurity;
module.exports.portUsers = portUsers;
module.exports.portAccounts = portAccounts;
module.exports.URI = URI;
